package com.uniajc.pqrspro.pqrspro_project.service;
import com.uniajc.pqrspro.pqrspro_project.dto.RolDto;
import com.uniajc.pqrspro.pqrspro_project.modelo.Rol;

import java.util.List;

public interface RolService {

    String registrar(RolDto rolDto);

    String actualizar(RolDto rolDto);

    String eliminar(Long id);

    Rol consultarRolById(Long id);

    List<Rol> consultarRoles();

}
