package com.uniajc.pqrspro.pqrspro_project.service.impl;

import com.uniajc.pqrspro.pqrspro_project.dto.PqrsDto;
import com.uniajc.pqrspro.pqrspro_project.modelo.Pqrs;
import com.uniajc.pqrspro.pqrspro_project.repository.PqrsRepository;
import com.uniajc.pqrspro.pqrspro_project.service.PqrsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PqrsServiceImpl implements PqrsService {

    @Autowired
    private PqrsRepository pqrsRepository;

    @Override
    public String registrar(PqrsDto pqrsDtoSave) {
        Pqrs dtoPqrsSave = new Pqrs();
        dtoPqrsSave.setId(pqrsDtoSave.getId());
        dtoPqrsSave.setIdestudiante(pqrsDtoSave.getIdestudiante());
        dtoPqrsSave.setNombreestudiante(pqrsDtoSave.getNombreestudiante());
        dtoPqrsSave.setArea(pqrsDtoSave.getArea());
        dtoPqrsSave.setDescripcion(pqrsDtoSave.getDescripcion());
        dtoPqrsSave.setEstado(pqrsDtoSave.getEstado());
        dtoPqrsSave.setCorreo(pqrsDtoSave.getCorreo());
        pqrsRepository.save(dtoPqrsSave);
        return "Registro Exitoso";
    }

    @Override
    public String actualizar(PqrsDto pqrsDtoUpdate) {
        Pqrs dtoPqrsUpdate = new Pqrs();
        dtoPqrsUpdate.setId(pqrsDtoUpdate.getId());
        dtoPqrsUpdate.setIdestudiante(pqrsDtoUpdate.getIdestudiante());
        dtoPqrsUpdate.setNombreestudiante(pqrsDtoUpdate.getNombreestudiante());
        dtoPqrsUpdate.setArea(pqrsDtoUpdate.getArea());
        dtoPqrsUpdate.setDescripcion(pqrsDtoUpdate.getDescripcion());
        dtoPqrsUpdate.setEstado(pqrsDtoUpdate.getEstado());
        dtoPqrsUpdate.setCorreo(pqrsDtoUpdate.getCorreo());
        pqrsRepository.save(dtoPqrsUpdate);
        return null;
    }

    @Override
    public String eliminar(Long id) {
        pqrsRepository.deleteById(id);
        return "Eliminado con Exito";
    }

    @Override
    public Pqrs consultarPqrsById(Long id) {
        return pqrsRepository.findById(id).orElse(null);
    }

    @Override
    public List<Pqrs> consultarPqrs() {
        return (List<Pqrs>) pqrsRepository.findAll();
    }
}
