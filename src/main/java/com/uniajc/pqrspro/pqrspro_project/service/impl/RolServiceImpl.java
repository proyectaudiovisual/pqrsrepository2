package com.uniajc.pqrspro.pqrspro_project.service.impl;

import com.uniajc.pqrspro.pqrspro_project.dto.RolDto;
import com.uniajc.pqrspro.pqrspro_project.modelo.Rol;
import com.uniajc.pqrspro.pqrspro_project.repository.RolRepository;
import com.uniajc.pqrspro.pqrspro_project.service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RolServiceImpl implements RolService {

    @Autowired
    private RolRepository rolRepository;

    @Override
    public String registrar(RolDto rolDtoSave) {
        Rol dtoRolUpdate = new Rol();
        dtoRolUpdate.setIdrol(rolDtoSave.getIdrol());
        dtoRolUpdate.setNombrerol(rolDtoSave.getNombre());
        rolRepository.save(dtoRolUpdate);
        return "Rol guardado con exito";
    }

    @Override
    public String actualizar(RolDto rolDtoUpdate) {
        Rol dtoRolUpdate = new Rol();
        dtoRolUpdate.setIdrol(rolDtoUpdate.getIdrol());
        dtoRolUpdate.setNombrerol(rolDtoUpdate.getNombre());
        rolRepository.save(dtoRolUpdate);
        return "Rol actualizado con exito";
    }

    @Override
    public String eliminar(Long id) {
        rolRepository.deleteById(id);
        return "Eliminado con exito";
    }

    @Override
    public Rol consultarRolById(Long id) {
        return rolRepository.findById(id).orElse(null);
    }

    @Override
    public List<Rol> consultarRoles() {
        return (List<Rol>) rolRepository.findAll();
    }
}
