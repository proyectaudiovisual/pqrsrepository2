package com.uniajc.pqrspro.pqrspro_project.controller;
import com.uniajc.pqrspro.pqrspro_project.dto.GenericDto;
import com.uniajc.pqrspro.pqrspro_project.dto.PqrsDto;
import com.uniajc.pqrspro.pqrspro_project.service.PqrsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/Pqrs")
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class PqrsController {

    @Autowired
    private PqrsService pqrsServices;

    @PostMapping("/registrar")
    @CrossOrigin(origins = "*")
    public ResponseEntity<GenericDto> registrar(@RequestBody PqrsDto pqrsDto) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.pqrsServices.registrar(pqrsDto)));
    }

    @PutMapping("/actualizar")
    @CrossOrigin(origins = "*")
    public ResponseEntity<GenericDto> actualizar(@RequestBody PqrsDto pqrsDto) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.pqrsServices.actualizar(pqrsDto)));
    }

    @DeleteMapping("/eliminar")
    @CrossOrigin(origins = "*")
    public ResponseEntity<GenericDto> eliminar(@RequestParam("id") Long id) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.pqrsServices.eliminar(id)));
    }

    @GetMapping("/consultarPqrsById")
    public ResponseEntity<GenericDto> consultarPqrsById(@RequestParam("id") Long id) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.pqrsServices.consultarPqrsById(id)));
    }

    @GetMapping("/consultarPqrs")
    public ResponseEntity<GenericDto> consultarPqrs() {
        return ResponseEntity.ok().body(GenericDto.sucess(this.pqrsServices.consultarPqrs()));
    }
}
