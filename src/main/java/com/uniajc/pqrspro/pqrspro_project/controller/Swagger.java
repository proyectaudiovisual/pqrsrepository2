package com.uniajc.pqrspro.pqrspro_project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public  class Swagger {
    @RequestMapping( "/swagger" )
    public  String  home () {
        return  " redirect: /swagger-ui.html" ;
    }
}