package com.uniajc.pqrspro.pqrspro_project.controller;

import com.uniajc.pqrspro.pqrspro_project.dto.GenericDto;
import com.uniajc.pqrspro.pqrspro_project.dto.RolDto;
import com.uniajc.pqrspro.pqrspro_project.service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Rol")
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class RolController {

    @Autowired
    private RolService rolService;

    @PostMapping("/registrar")
    @CrossOrigin(origins = "*")
    public ResponseEntity<GenericDto> registrar(@RequestBody RolDto rolDto) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.rolService.registrar(rolDto)));
    }

    @PutMapping("/actualizar")
    @CrossOrigin(origins = "*")
    public ResponseEntity<GenericDto> actualizar(@RequestBody RolDto rolDto) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.rolService.actualizar(rolDto)));
    }

    @DeleteMapping("/eliminar")
    @CrossOrigin(origins = "*")
    public ResponseEntity<GenericDto> eliminar(@RequestParam("id") Long id) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.rolService.eliminar(id)));
    }

    @GetMapping("/consultarRolById")
    public ResponseEntity<GenericDto> consultarRolById(@RequestParam("id") Long id) {
        return ResponseEntity.ok().body(GenericDto.sucess(this.rolService.consultarRolById(id)));
    }

    @GetMapping("/consultarRoles")
    public ResponseEntity<GenericDto> consultarRoles() {
        return ResponseEntity.ok().body(GenericDto.sucess(this.rolService.consultarRoles()));
    }
}
