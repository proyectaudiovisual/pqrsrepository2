package com.uniajc.pqrspro.pqrspro_project.repository;

import com.uniajc.pqrspro.pqrspro_project.modelo.Pqrs;
import org.springframework.data.repository.CrudRepository;

public interface PqrsRepository extends CrudRepository<Pqrs, Long> {
}
