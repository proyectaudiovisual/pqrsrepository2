package com.uniajc.pqrspro.pqrspro_project.dto;

import java.io.Serializable;

public class PqrsDto implements Serializable {

    private Long id;
    private Long idestudiante;
    private String nombreestudiante;
    private String area;
    private String descripcion;
    private String estado;
    private String correo;

    public Long getId() {
        return this.id;
    }

    public void setId(Long idSet) {
        this.id = idSet;
    }

    public Long getIdestudiante() {
        return this.idestudiante;
    }

    public void setIdestudiante(Long idestudianteSet) {
        this.idestudiante = idestudianteSet;
    }

    public String getNombreestudiante() {
        return this.nombreestudiante;
    }

    public void setNombreestudiante(String nombreestudianteSet) {
        this.nombreestudiante = nombreestudianteSet;
    }

    public String getArea() {
        return this.area;
    }

    public void setArea(String areaSet) {
        this.area = areaSet;
    }

    public String getCorreo() {
        return this.correo;
    }

    public void setCorreo(String correoSet) {
        this.correo = correoSet;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
