package com.uniajc.pqrspro.pqrspro_project.dto;

import java.io.Serializable;

public class RolDto implements Serializable {

    private Long idrol;
    private String nombre;


    public Long getIdrol() {
        return idrol;
    }

    public void setIdrol(Long idrol) {
        this.idrol = idrol;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombreSet) {
        this.nombre = nombreSet;
    }

    @Override
    public String toString() {
        return "RolDto{" +
                "idrol=" + idrol +
                ", nombre='" + nombre + '\'' +
                '}';
    }


}
